% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/utilsPlotting.R
\name{utilPlotMixOmicsHeatmap}
\alias{utilPlotMixOmicsHeatmap}
\title{utilPlotMixOmicsHeatmap}
\usage{
utilPlotMixOmicsHeatmap(model)
}
\arguments{
\item{model}{Model objects obtained from training.}
}
\description{
Creates a sample heatmap for a \code{mixOmics} factor model.
}
\details{
The plot is drawn using the \code{cimDiablo} function of the
\code{mixOmics} package (Rohart et al. 2017).
}
\references{
Rohart, F., Gautier, B., Singh, A., & Le Cao, K. A. (2017). mixOmics:
An R package for omics feature selection and multiple data integration.
PLoS computational biology, 13(11), e1005752.
}
\author{
Benjamin Ulfenborg
}
