---
title: "Miodin Developer Manual"
subtitle: "Version 0.5.4"
author: "Benjamin Ulfenborg, PhD"
date: "12th May 2021"
output:
  prettydoc::html_pretty:
    toc: TRUE
vignette: >
  %\VignetteIndexEntry{Miodin Developer Manual}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# About this manual

This manual is geared towards those that want to develop extensions to the
`miodin` package. The text does not assume extensive knowledge of the
design of the package, but familiarity with R and the S4 object oriented
paradigm is required. A great place to start learning is the online book
[Advanced R](http://adv-r.had.co.nz) by Hadley Wickham. The chapter on object
oriented programming gives a good overview of S4.

# The anatomy of a workflow module

The functionality provided by `miodin` to import, process, integrate and analyze
multi-omics data is wrapped into workflow modules. Each module carries out a
dedicated step of the analysis and serves as an abstraction layer between the
underlying analysis code and the user. The rationale behind this is that the
user should only be concerned with what main analysis steps that need to be
carried out, and supply necessary parameters. What packages, functions and
classes that are required, and technical steps need to be considered, should
be handled seamlessly by the module and functions that serve it. In this way,
`miodin` allows users to build streamlined workflows that are more transparent
compared to long analysis scripts, while promoting reproducibility, scalability
and tracking of data provenance. Extending the functionality of the package will
thus require development of new workflow modules, or extensions to existing
ones. The following sections outline the internal structure of modules and what
steps are needed to define a new module. This includes specifying

* The class definition
* Input and output ports
* The execution method
* A module instantiator
* Documentation

The quickest way to get started implementing a new workflow module is to make
a copy of the ModuleSkeleton.R code in the `miodin` package. This contains most
of the essential component required by any module.

### Class definition

The first thing to specify is the S4 class definition by calling `setClass`.
Its first argument is the name of the new module, which must be unique. The
remaining parameters to set are `contains`, `prototype` and `sealed`. It is
required for all modules to inherit from the class `MiodinWorkflowModule`,
since this defines necessary properties and methods expected by all modules.
This is specified by `contains = "MiodinWorkflowModule"`. With `prototype` we
define the default values of the slots in the S4 class, here inherited from
`MiodinWorkflowModule`. There are two slots that must be specified:
`parameterValues`, which is equal to a list of parameters accepted and their
default values, and `instantiator`, equal to the name of the module instantiator
function. The `sealed` parameter should be set to `TRUE` to prevent the class
definition from being overwritten by e.g. another package later on. An example
class definition is given below.

```{r, eval = FALSE}
MyModule <- setClass(
  "MyModule",
  prototype = list(
    parameterValues = list(
      parameter1 = 1,
      parameter2 = "a"
    ),
    instantiator = "myInstantiator"
  ),
  contains = "MiodinWorkflowModule",
  sealed = TRUE
)
```

### Input and output ports

Modules in a `miodin` workflow communicate using so-called data interfaces.
These are one-way channels that pass data from an upstream to a downstream
module and are created by connecting two modules using the pipe operator `%>%`.
A data interface can only be connected between modules with compatible input
and output ports. A port is a declaration made by the module what input is
expects and what output it promises to be deliver when it is finished. Each port
has a set of slots with a defined class, e.g. `MiodinDataset` or `character`.
To be compatible, two ports must have slots with the same classes, so that data
in the upstream output port can be passed into the downstream input port. This
check is made automatically by the pipe operator when a workflow is defined. To
be able to map output ports to the corresponding input ports, standard port and
slot names are used for different types of data. Table 1 and 2 below summarize
the currently used standard port and slot names.

Table 1: Standard port names used for input and output ports

```{r, echo = FALSE}
df <- data.frame(
  PortName = c("dataset", "analysisResult"),
  Description = c("Used for ports that contain a MiodinDataset",
                  "Used for ports that contain a MiodinAnalysisResult")
)
colnames(df)[1] <- "Port Name"
knitr::kable(df)
```

Table 2: Standard slot names used in input and output ports

```{r, echo = FALSE}
df <- data.frame(
  SlotName = c("mds", "assayName", "mar"),
  Description = c("A MiodinDataset",
                  "Character vector of one or more assay names",
                  "A MiodinAnalysisResult")
)
colnames(df)[1] <- "Slot Name"
knitr::kable(df)
```

Modules are not required to have input and output ports. A module that only
downloads data may have neither, and a module that inports data may only have
an output port. Processing and analysis modules tend to have both an input and
output port, as they must accept data to analyze and provide the results as
output. Ports are defined in the `initialize` function, which is automatically
called when the module is instantiated. This example illustrates how the input
and output ports of the `OmicsModeler` module are defined. Each port added to
the module (`.Object`) has a name, slots and slot descriptions.

```{r, eval = FALSE}
setMethod("initialize", "OmicsModeler",
  function(.Object, ...){
    .Object <- .Object +
      inputDataPort(
        name = "dataset",
        slots = c(mds = "MiodinDataset", assayName = "character"),
        slotDesc = c(mds = "MiodinDataset containing assay data",
                     assayName = "Names of assays to analyze")
      ) +
      outputDataPort(
        name = "analysisResult",
        slots = c(mar = "MiodinAnalysisResult"),
        slotDesc = c(mar = "MiodinAnalysisResult containing results from analysis")
      )
    return(.Object)
  } )
```

Here we define two ports, and input port called `dataset` and an output port
called `analysisResult`. The input port has two slots (`mds` and `assayName`),
while the output port only has one (`mar`). Another argument that can be passed
to `inputDataPort` is `multiPort`. This is a logical value indicating if that
port should accept input from several output ports simultanlusly. The default
is `FALSE` and this is only set to `TRUE` for special modules like
`integrateAssays`, which need to take several datasets and merge them into one.

### Execution method



### Instantiator



### Documentation

Export class definition, initialize and the module instantiator (not execute)

[documentation](http://r-pkgs.had.co.nz/man.html)

# Helper functions

### Processing, annotation and analysis environments



### How to define util functions



### utilImporter*



### utilProcessor*



### utilQuality*



### utilPlot*



### utilAnalysis*



### How to define proc functions



# An example module



# Registering the module in a project



# Executing the module

