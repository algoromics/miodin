# Miodin: An R package for multi-omics data integration

The `miodin` package provides a software infrastructure for declaring
study designs and executing data analysis workflows in R. The primary
audience for the package are those who want to perform multi-omics data
integration, either across studies on the same set of variables or
across different omics modalities on the same samples. An important
design goal was to render data analysis as streamlined as possible, to
make data integration easy, and promote transparent and reproducible
biomedical data science.

More in-depth description of how to use the `miodin` package can be
found in the [user manual](https://algoromics.gitlab.io/miodin). For
complete description of classes and method, see the [reference
documentation](https://gitlab.com/algoromics/miodin/raw/master/miodin.pdf).

# News

\[8/3 2024\] Version 0.6.2 released

\[10/2 2022\] Version 0.6.1 released

\[10/2 2022\] Version 0.6.0 released

# Installation

The easiest way to install `miodin` is to run the following code in R.

``` r
if(!require(BiocManager)) install.packages("BiocManager")
if(!require(devtools)) install.packages("devtools")
BiocManager::install(
  c('restfulr', 'org.Hs.eg.db', 'MultiDataSet', 'Biobase', 'BiocGenerics',
    'oligo', 'snpStats', 'GenomeInfoDbData', 'GenomeInfoDb', 'DMRcatedata',
    'crlmm', 'limma', 'minfi', 'SNPRelate', 'wateRmelon', 'DMRcate', 'IRanges',
    'SummarizedExperiment', 'GenomicRanges', 'DESeq2', 'MSnbase', 'edgeR',
    'qvalue', 'mixOmics', 'MOFA2'), ask = FALSE)
install.packages("https://cran.r-project.org/src/contrib/Archive/RefFreeEWAS/RefFreeEWAS_2.2.tar.gz", repos = NULL)
devtools::install_git(url = "https://gitlab.com/wolftower/biostudies.git")
devtools::install_git(url = "https://gitlab.com/algoromics/miodin.git")
devtools::install_git(url = "https://gitlab.com/algoromics/miodindata.git")
```

To install the latest development version of the package, set
`ref = "devel"`.

``` rm
devtools::install_git(url = "https://gitlab.com/algoromics/miodin.git", ref = "devel")
```

# Run in web browser without installing

To facilitate package usage, and relieve users from installing all
dependencies, a [Docker](https://www.docker.com) image has been prepared
that supports [RStudio](https://rstudio.com) in the web browser. All
that is needed is to install Docker, pull the image from
[DockerHub](https://hub.docker.com) and run a container in the terminal.
Instructions for installing Docker can be found
[here](https://docs.docker.com/install/).

If you are using Mac you may need to increase the memory limit in the
Deamon. Check
[this](https://stackoverflow.com/questions/44533319/how-to-assign-more-memory-to-docker-container/44533437#44533437)
entry to see how. It is recommended to run `miodin` with at least 4 GiB
allocated.

To pull the Docker image containing `miodin` and all its dependencies,
run the following command in the terminal.

    docker pull wolftower85/miodinverse:latest

When the image has been downloaded you can use it to start the
container.

    docker run -e DISABLE_AUTH=true --rm -p 8787:8787 wolftower85/miodinverse:latest

Now visit `localhost:8787` in the web browser and RStudio will start.
You can then run `library(miodin)` to load the package without
installing any dependencies.

Since you are running RStudio in a container, you cannot see the file
system on your computer. To access the file system, you need to mount a
path in the container pointing to a folder on your computer. This is
done when the container is started.

    docker run -e DISABLE_AUTH=true -v PATH_ON_DISK:/home/rstudio/volume --rm -p 8787:8787 wolftower85/miodinverse:latest

Replace `PATH_ON_DISK` with a path to a folder where you want to save
results generated in the container. When working in RStudio in the web
browser, everything saved in the `volume` folder will be available in
your regular file system.

# Citation

If you use the miodin package in publications, please cite the following
paper.

Ulfenborg, B. Vertical and horizontal integration of multi-omics data
with miodin. BMC Bioinformatics 20, 649 (2019)
[doi:10.1186/s12859-019-3224-4](https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-019-3224-4)

# Contact and support

Questions can be sent to the package maintainer Benjamin Ulfenborg
(<benjamin.ulfenborg@his.se>). For user support, bugs and suggestions on
improving the package, you can also use the repository [Issue
Tracker](https://gitlab.com/algoromics/miodin/issues).
