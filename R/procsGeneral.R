#' @include utilsDataProcessing.R
NULL

#' @title procMapID
#' 
#' @description
#' Processing function for mapping identifiers to Entrez gene IDs
#' 
#' @param mds A \code{MiodinDataset} for each features will be mapped.
#' @param assayName Name of assay for which features will be mapped.
#' @param inputIdentifier The input accession type.
#' 
#' @details
#' 
#' The function accepts \code{inputIdentifier} including,
#' \code{"symbol"} (official HGNC), \code{"entrez"}, \code{"uniprot"}
#' and \code{"ensg"} (Ensembl gene IDs). These are mapped by the
#' \code{org.Hs.eg.db} package to the corresponding Entrez IDs, which
#' are added as a column to the feature annotations in the dataset.
#' 
#' @return
#' A data processing environment.
#' 
#' @author Benjamin Ulfenborg
#' 
#' @export
procMapID <- function(mds, assayName, inputIdentifier){
  
  #Validate identifier
  if(!inputIdentifier %in% c("symbol", "entrez", "uniprot", "ensg"))
    stop("inputIdentifier must be one of symbol, entrez, uniprot or ensg")
  if(inputIdentifier == "symbol") inputIdentifier <- "SYMBOL"
  if(inputIdentifier == "entrez") inputIdentifier <- "ENTREZID"
  if(inputIdentifier == "uniprot") inputIdentifier <- "UNIPROT"
  if(inputIdentifier == "ensg") inputIdentifier <- "ENSEMBL"
  
  #Map identifiers to organism database
  mappedEntrez <- tryCatch({
    suppressMessages(
      AnnotationDbi::mapIds(
        x = org.Hs.eg.db::org.Hs.eg.db,
        keys = rownames(mds[[assayName]]),
        column = "ENTREZID",
        keytype = inputIdentifier,
        multiVals = "first"))
  }, error = function(e) {
    stop("Rownames in assay ", assayName, " is not of type ", inputIdentifier)
    }
  )
  suppressMessages(
    mappedSymbol <- AnnotationDbi::mapIds(
      x = org.Hs.eg.db::org.Hs.eg.db,
      keys = rownames(mds[[assayName]]),
      column = "SYMBOL",
      keytype = inputIdentifier,
      multiVals = "first"))
  mappedIDs <- data.frame(Entrez = mappedEntrez, Symbol = mappedSymbol)
  mds <- add(mds, "featureData", assayName, mappedIDs)
  
  #Add annotation step to dataset
  annotEnv <- utilMakeProcEnv(desc = "Mapping identifiers to Entrez and HGNC",
                              procName = "procAnnotateProbes",
                              procPackage = "miodin",
                              procVersion = packageVersion("miodin"),
                              inputIdentifier = inputIdentifier,
                              database = "org.Hs.eg.db")
  annotEnv$mds <- mds
  return(annotEnv)
}
