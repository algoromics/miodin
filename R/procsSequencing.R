#' @include utilsDataProcessing.R
NULL

#' @title procFilterLowCounts
#' 
#' @description
#' Processing function to filter features with low counts.
#' 
#' @param filterLowCountThresh Threshold for mean log2 CPM.
#' @param filterLowCountNumObs This parameter is deprecated.
#' 
#' @details
#' 
#' Count data is filtered row-wise to remove rows where the row counts are too
#' low (i.e. low expression). A row will be retained if the mean log2 CPM is
#' above \code{filterLowCountThresh}.
#' 
#' @return
#' A data processing environment.
#' 
#' @author Benjamin Ulfenborg
#' 
#' @export
procFilterLowCounts <- function(filterLowCountThresh, filterLowCountNumObs){
  procFun <- function(data){
    if(is(data, "SummarizedExperiment"))
      dataMat <- SummarizedExperiment::assay(data)
    else if(is(data, "MSnSet"))
      dataMat <- Biobase::exprs(data)
    else{
      warning("Unable to filter low-counts on data object", class(data))
      return(data)
    }
    meanLog2CPM <- rowMeans(log2(edgeR::cpm(data) + 1))
    keep <- meanLog2CPM > filterLowCountThresh
    return(data[keep, ])
  }
  procEnv <- utilMakeProcEnv(desc = "Filtering out low-count features",
                             procName = "procFilterLowCounts",
                             procPackage = "miodin",
                             procVersion = packageVersion("miodin"),
                             procFun = procFun,
                             lowExpThresh = filterLowCountThresh,
                             lowExpNumObs = filterLowCountNumObs)
  return(procEnv)
}

#' @title procVarianceStabilization
#' 
#' @description
#' Processing function to stabilize variance with the \code{DESeq2} package.
#' 
#' @param mds A \code{MiodinDataset}.
#' @param assayObj Assay object for which the design matrix will be created.
#' @param assayName The name of an assay.
#' @param contrastName The name of the contrast to include in the design matrix.
#' @param studies A list of \code{MiodinStudy} objects containing the study used
#' to import the assay.
#' @param sampleTable Sample table for the assay.
#' @param covariates List of vectors, where list names are names of assays and
#' vectors contain covariates of that assay to adjust for.
#' @param method Method of transformation, either \code{"rlog"} or
#' \code{"vst"}.
#' 
#' @details
#' 
#' Count data is normalized to stabilize variance, i.e. remove
#' heteroscedasticity, where high-expressed variables will have higher
#' variance simply because they are high-expressed. The normalization
#' method is specified with \code{method} and is either rlog or vst,
#' both implemented in the \code{DESeq2} package (Love et al. 2014).
#' 
#' @return
#' A data processing environment.
#' 
#' @author Benjamin Ulfenborg
#' 
#' @references
#' 
#' Love, M. I., Huber, W., & Anders, S. (2014). Moderated estimation of
#' fold change and dispersion for RNA-seq data with DESeq2. Genome biology,
#' 15(12), 550.
#' 
#' @export
procVarianceStabilization <- function(mds, assayName, contrastName, studies,
                                      sampleTable, covariates, method = "rlog"){
  procFun <- function(data){
    if(!(method %in% c("rlog", "vst")))
      stop("Unknown transformation ", method)
    if(is.null(colnames(data)))
      colnames(data) <- sampleTable$SampleName
    suppressWarnings(
      designRes <- utilMakeDesignMatrix(mds = mds,
                                        assayObj = data,
                                        assayName = assayName,
                                        contrastName = contrastName,
                                        studies = studies,
                                        sampleTable = sampleTable,
                                        covariates = covariates))
    if(is.null(designRes$designMatrix))
      design <- ~ 1
    else
      design <- designRes$designMatrix
    dds <- DESeq2::DESeqDataSet(data, design = design)
    dds <- dds[rowSums(DESeq2::counts(dds)) >= 1, ]
    if(method == "rlog")
      transMatrix <- DESeq2::rlog(dds, blind = FALSE)
    else if(method == "vst"){
      transMatrix <- tryCatch({
        DESeq2::vst(dds, nsub = ifelse(nrow(dds) < 1000, nrow(dds), 1000),
                    blind = FALSE)
      }, error = function(e) { e$message } )
      if(is.character(transMatrix))
        transMatrix <- DESeq2::varianceStabilizingTransformation(dds,
                                                                 blind = FALSE)
    }
    colnames(data) <- SummarizedExperiment::colData(data)$SampleName
    colnames(transMatrix) <- SummarizedExperiment::colData(data)$SampleName
    if(!all(rownames(data) %in% rownames(transMatrix)))
      data <- data[rownames(transMatrix), ]
    if(!is.matrix(transMatrix))
      transMatrix <- SummarizedExperiment::assay(transMatrix)
    SummarizedExperiment::assays(data)$trans <- transMatrix
    return(data)
  }
  procEnv <- utilMakeProcEnv(desc = "Performing variance stabilization",
                             procName = "procVarianceStabilization",
                             procPackage = "miodin",
                             procVersion = packageVersion("miodin"),
                             procFun = procFun)
  return(procEnv)
}
