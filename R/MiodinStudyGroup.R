#Include MiodinStudyGroup dependencies
#' @include standardGenerics.R
NULL

#' @title MiodinStudyGroup
#' 
#' @description
#' 
#' The \code{MiodinStudyGroup} class represents a single group of samples.
#' 
#' \preformatted{## Class constructor
#' MiodinStudyGroup()}
#' 
#' @param object A \code{MiodinStudyGroup} object.
#' @param props Named list of properties to set (for \code{setProp}) or a
#' character vector (for \code{getProp}).
#' @param res Named used to return parameters.
#' @param factors List of \code{MiodinStudyFactor} objects.
#' @param samplingPoints Character vector of sampling points.
#' @param validate Whether to run validation code.
#' @param factorName Name of factor removed.
#' 
#' @details
#' 
#' This is one of three classes used in the \code{MiodinStudiDesign} to declare
#' the design of a factorial experiment. The class is used internally and never
#' explicitly instantiated by the user.
#' 
#' @section Properties of \code{MiodinStudyGroup}:
#' 
#' The following property can be set/retrieved with
#' \code{setProp} and \code{getProp}.
#' \itemize{
#'   \item \code{"grouFactors"}: Data frame with all factor level-sampling
#'   points included in the group.
#' }
#'
#' @slot factors Data frame with all factor level-sampling points included in
#' the group.
#' 
#' @author Benjamin Ulfenborg
#' 
#' @name MiodinStudyGroup-class
#' @export MiodinStudyGroup
MiodinStudyGroup <- setClass(
  "MiodinStudyGroup",
  slots = list(
    factors = "data.frame"
  ),
  prototype = list(
    factors = data.frame()
  ),
  validity = function(object){
    errors <- character()
    return(TRUE)
  },
  sealed = TRUE
)

#' @describeIn MiodinStudyGroup Sets properties of the group.
setMethod("setProp", "MiodinStudyGroup",
  function(object, props, factors = NULL, samplingPoints = NULL,
           validate = TRUE){
    if(!class(props)[1] == "list") stop("props must be a list")
    if(length(props) == 0) stop("props must not be empty")
    if(is.null(names(props))) stop("props must be a named list")
    for(prop in names(props)){
      if(!(prop %in% c("groupFactors")))
        stop("Unknown group property ", prop)
      value <- props[[prop]]
      if(prop == "groupFactors"){
        if(!is.data.frame(value))
          stop("groupFactors must be a data frame")
        if(any(!(c("Factor", "Level", "SamplingPoint") %in% colnames(value))))
          stop("groupFactors must include columns Factor, ",
               "Level and SamplingPoint")
        if(any(duplicated(value)))
          stop("Duplicate factors in contrastGroups")
        if(validate){
          if(is.null(factors) || is.null(samplingPoints))
            stop("factors and samplingPoints must be given when validate ",
                 "is TRUE")
          value <- data.frame(lapply(value, as.character),
                              stringsAsFactors = FALSE)
          if(any(!(value$Factor %in% names(factors))))
            stop("Invalid factor(s) in groupFactors")
          for(g in seq_len(nrow(value))){
            if(!(value$Level[g] %in% getProp(factors[[value$Factor[g]]],
                                             "levels")[["levels"]]))
              stop("Invalid level ", value$Level[g], " for factor ",
                   value$Factor[g], " in groupFactors")
          }
          if(any(!(value$SamplingPoint %in% samplingPoints)))
            stop("Invalid sampling point(s) in groupFactors")
        }
        object@factors <- data.frame(value, stringsAsFactors = FALSE)
      }
    }
    return(object)
  } )

#' @describeIn MiodinStudyGroup Returns properties of the group.
setMethod("getProp", "MiodinStudyGroup",
  function(object, props, res = list()){
    if(length(props) == 0)
      stop("props must not be empty")
    if(!is.character(props))
      stop("props must be a character vector")
    for(prop in props){
      if(!(prop %in% c("groupFactors")))
        stop("Unknown group property ", prop)
      if(prop == "groupFactors")
        res[["groupFactors"]] <- object@factors
    }
    return(res)
  } )

#' @describeIn MiodinStudyGroup Synchronize group factors in response to
#' removing factors from the study design.
setMethod("sync", "MiodinStudyGroup",
  function(object, factorName){
    ind <- which(object@factors$Factor == factorName)
    if(length(ind) > 0){
      if(nrow(object@factors) == 1)
        object@factors <- data.frame()
      else
        object@factors <- object@factors[-ind, ]
    }
    return(object)
  } )
