#Include DataRepositoryInterface dependencies
#' @include standardGenerics.R MiodinWorkflowModule.R
NULL

#' @title DataRepositoryInterface
#' 
#' @description
#' Workflow module class for downloading data from an online repository.
#' 
#' @details
#' 
#' The dataset is specified by accession number and is downloaded to the given
#' path on disk. The workflow module has no input or output ports.
#' 
#' @author Benjamin Ulfenborg
#' 
#' @seealso \link{MiodinWorkflowModule-class}
#' 
#' @name WorkflowModule-DataRepositoryInterface
#' @export DataRepositoryInterface
DataRepositoryInterface <- setClass(
  "DataRepositoryInterface",
  prototype = list(
    parameterValues = list(
      repository = "ArrayExpress",
      accession = "",
      path = "",
      type = "raw"
    ),
    documentation = "",
    priority = TRUE,
    instantiator = "downloadRepositoryData"
  ),
  validity = function(object){
    errors <- character()
    if(length(errors) == 0)
      return(TRUE)
    else
      return(errors)
  },
  contains = "MiodinWorkflowModule",
  sealed = TRUE
)

#' @export
setMethod("initialize", "DataRepositoryInterface",
  function(.Object, ...){
    .Object <- setProp(.Object, list(
      parameterDesc = list(Names = data.frame(
        Parameter = c("repository", "accession", "path"),
        Data = c("Character", "Character", "Character"),
        Mandatory = c("Yes", "Yes", "No"),
        Description = c(
          "Repository from which to download data (ArrayExpress or GDC)",
          "Dataset accession number",
          "Path where data is saved"))
      )
    ))
    return(.Object)
  } )

setMethod("execute", "DataRepositoryInterface",
  function(object, studies, projectPath, workflowConfiguration,
           inputPorts, verbose, moduleName, repository, accession,
           path, type){
    if(!(repository %in% c("BioStudies")))
      stop("Unknown repository", repository)
    if(!requireNamespace("BioStudies"))
      stop("The BioStudies package is required for downloading repository data")
    if(accession == "")
      stop("Accession number must be a non-empty string")
    if(!dir.exists(path)) dir.create(path, recursive = TRUE)
    if(repository == "BioStudies"){
      res <- tryCatch(BioStudies::getBio(accession = accession,
                                         path = path),
               error = function(e) { e$message } )
      if(is.character(res))
        stop("Download failed with the following error: ", res)
    }
    return(list())
  } )

#' @title downloadRepositoryData
#' 
#' @description
#' Workflow module for downloading omics data from online repositories.
#' 
#' @param repository Online repository from where to download data. Currently
#' only supports \code{"BioStudies"}.
#' @param accession Accession number of the dataset to download.
#' @param path Target path where data will be saved.
#' 
#' @details
#' ArrayExpress is an online repository for high-throughput functional genomics
#' data (\url{https://www.ebi.ac.uk/arrayexpress/}). Raw data is downloaded by
#' giving a dataset accession number with \code{accession} and a download
#' path with \code{path}. To find a suitable dataset, browse or search the
#' database online. Accession numbers have the form E-MTAB-XXXX or E-GEOD-XXXX.
#' Downloaded microarray data can then be imported with
#' \code{importMicroarrayData} (Affymetrix and Illumina is supported).
#' 
#' @author Benjamin Ulfenborg
#' 
#' @seealso \link{WorkflowModule-MicroarrayDataImporter}
#' 
#' @name InstatiateModule-downloadRepositoryData
#' @export downloadRepositoryData
downloadRepositoryData <- function(name,
                                   repository = "ArrayExpress",
                                   accession = "",
                                   path = "",
                                   type = "raw"){
  mwm <- workflowModule("DataRepositoryInterface", name,
                        repository = repository,
                        accession = accession,
                        path = path,
                        type = type)
  return(mwm)
}
