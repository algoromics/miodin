#Include ProcessedDataImporter dependencies
#' @include standardGenerics.R MiodinWorkflowModule.R
#' @include utilsProcessedData.R utilsMicroarray.R utilsSequencing.R
#' @include utilsMassSpec.R
NULL

#' @title ProcessedDataImporter
#' 
#' @description
#' Workflow module class for importing processed omics data.
#' 
#' @details
#' The module currently supports processed data from microarrays (RNA, SNP and
#' methylation), sequencing (RNA) and mass spectrometry (protein).
#' 
#' @section Input ports:
#' 
#' The module has not input ports.
#' 
#' @section Output ports:
#' 
#' The module has a single output port:
#' \itemize{
#'   \item \code{dataset}: This contains two slots, \code{mds} and
#'   \code{assayName}, which contain the \code{MiodinDataset} and assay name,
#'   respectively.
#' }
#' 
#' @author Benjamin Ulfenborg
#' 
#' @name WorkflowModule-ProcessedDataImporter
#' @export ProcessedDataImporter
ProcessedDataImporter <- setClass(
  "ProcessedDataImporter",
  prototype = list(
    parameterValues = list(
      experiment = "",
      dataType = "",
      studyName = "",
      assayName = "",
      datasetName = "",
      contrastName = NULL,
      chipName = "",
      probeAnnotationFile = NULL,
      probeSummarization = "transcript",
      useff = FALSE,
      orderedByFeature = FALSE,
      methylValues = "M",
      inputIdentifier = "",
      labelSamples = FALSE,
      excludeSamples = NULL
    ),
    documentation = "",
    instantiator = "importProcessedData"
  ),
  validity = function(object){
    errors <- character()
    if(length(errors) == 0)
      return(TRUE)
    else
      return(errors)
  },
  contains = "MiodinWorkflowModule",
  sealed = TRUE
)

#' @export
setMethod("initialize", "ProcessedDataImporter",
  function(.Object, ...){
    .Object <- .Object +
      outputDataPort(
        name = "dataset",
        slots = c(mds = "MiodinDataset", assayName = "character"),
        slotDesc = c(mds = "MiodinDataset containing assay data",
                     assayName = "Names of assays to analyze")
      )
    return(.Object)
  } )

setMethod("execute", "ProcessedDataImporter",
  function(object, studies, projectPath, workflowConfiguration,
           inputPorts, experiment, dataType, studyName, assayName,
           datasetName, contrastName, chipName, probeAnnotationFile,
           probeSummarization, useff, orderedByFeature, methylValues,
           inputIdentifier, labelSamples, excludeSamples, verbose, moduleName){
    
    #Validate arguments
    if(!(experiment %in% c("microarray", "sequencing", "mass spec",
                           "olink", "assay")))
      stop("experiment must be 'microarray', 'sequencing', 'mass spec', ",
           "'olink' or 'assay'")
    if(!(dataType %in% c("rna", "snp", "methylation", "protein", "mutation",
                         "metabolite", "generic")))
      stop("dataType must be either 'rna', 'snp', 'methylation', 'protein', ",
           "'mutation', 'metabolite' or 'generic'")
    if(studyName == "" || !is.character(studyName))
      stop("studyName must be a non-empty character string")
    if(!(studyName %in% names(studies)))
      stop("Study ", studyName, " does not exist")
    if(assayName == "" || !is.character(assayName))
      stop("assayName must be a non-empty character string")
    if(!(assayName %in% names(getAll(studies[[studyName]], "assayTable"))))
      stop("Assay table ", assayName, " does not exist")
    if(datasetName == "" || !is.character(datasetName))
      stop("datasetName must be a non-empty character string")
    if(!is.logical(labelSamples))
      stop("labelSamples must be TRUE or FALSE")
    
    #Retrieve sample table
    sampleTable <- getProp(get(studies[[studyName]],
                               "sampleTable"), "table")[["table"]]
    
    #Retrieve assay table
    assayTable <- get(studies[[studyName]], "assayTable", assayName)
    assayTable <- getProp(assayTable, "table")[["table"]]
    if(!("SampleName" %in% colnames(assayTable)))
      stop("Assay table ", assayName, " is missing the SampleName column")
    if(!("DataFile" %in% colnames(assayTable)))
      stop("Assay table ", assayName, " is missing the DataFile column")
    if(!("DataColumn" %in% colnames(assayTable)))
      stop("Assay table ", assayName, " is missing the DataColumn column")
    samplesInc <- which(!assayTable$DataFile %in% c("", NA))
    assayTable <- assayTable[samplesInc, , drop = FALSE]
    sampleIncNames <- unique(assayTable$SampleName)
    if(any(is.na(match(sampleIncNames, sampleTable$SampleName))))
      stop("Sample names in assay table are missing in the sample table")
    sampleTable <- sampleTable[match(sampleIncNames, sampleTable$SampleName), ,
                               drop = FALSE]
    if(nrow(assayTable) == 0)
      stop("No assay files to import")
    
    #Determine assay file format
    fileFormat <- ""
    if(length(unique(assayTable$DataFile)) == 1){
      fileFormat <- "oneFileSampleColumns"
      if(length(unique(assayTable$DataColumn)) == 1)
        fileFormat <- "oneFileSingleColumn"
    } else if(sum(duplicated(assayTable$DataFile)) == 0)
      fileFormat <- "oneFilePerSample"
    else if(length(unique(assayTable$DataFile)) > 1){
      fileFormat <- "batchFilesSampleColumns"
      if(length(unique(assayTable$DataColumn)) == 1)
        fileFormat <- "batchFilesSingleColumn"
    } else
      stop("Unable to determine assay file format")
    
    #Experiment-specific processing
    if(experiment == "microarray"){
      if(dataType == "rna" || dataType == "protein"){
        miodinDataset <- utilImporterProcessedMicroarrayRNA(
          datasetName = datasetName,
          assayTable = assayTable,
          assayName = assayName,
          studyName = studyName,
          sampleTable = sampleTable,
          chipName = chipName,
          probeSummarization = probeSummarization,
          projectPath = projectPath,
          probeAnnotationFile = probeAnnotationFile,
          workflowConfiguration = workflowConfiguration,
          verbose = verbose,
          moduleName = moduleName,
          fileFormat = fileFormat,
          useff = useff,
          orderedByFeature = orderedByFeature,
          inputIdentifier = inputIdentifier,
          excludeSamples = excludeSamples
        )
        miodinDataset <- utilQualityMicroarrayRNA(
          miodinDataset = miodinDataset,
          assayName = assayName,
          qcName = "Normalized data",
          contrastName = contrastName,
          studies = studies,
          verbose = verbose,
          labelSamples = labelSamples)
      } else if(dataType == "snp"){
        miodinDataset <- utilImporterProcessedMicroarraySNP(
          datasetName = datasetName,
          assayTable = assayTable,
          assayName = assayName,
          studyName = studyName,
          sampleTable = sampleTable,
          chipName = chipName,
          projectPath = projectPath,
          probeAnnotationFile = probeAnnotationFile,
          workflowConfiguration = workflowConfiguration,
          verbose = verbose,
          moduleName = moduleName,
          fileFormat = fileFormat,
          useff = useff,
          orderedByFeature = orderedByFeature,
          excludeSamples = excludeSamples
        )
        miodinDataset <- utilQualityMicroarraySNP(
          miodinDataset = miodinDataset,
          assayName = assayName,
          qcName = "Normalized data",
          studies = studies,
          projectPath = projectPath,
          contrastName = contrastName,
          verbose = verbose,
          labelSamples = labelSamples)
      } else if(dataType == "methylation"){
        miodinDataset <- utilImporterProcessedMicroarrayMethyl(
          datasetName = datasetName,
          assayTable = assayTable,
          assayName = assayName,
          studyName = studyName,
          sampleTable = sampleTable,
          chipName = chipName,
          projectPath = projectPath,
          probeAnnotationFile = probeAnnotationFile,
          workflowConfiguration = workflowConfiguration,
          verbose = verbose,
          moduleName = moduleName,
          fileFormat = fileFormat,
          useff = useff,
          orderedByFeature = orderedByFeature,
          methylValues = methylValues,
          excludeSamples = excludeSamples
        )
        miodinDataset <- utilQualityMicroarrayMethyl(
          miodinDataset = miodinDataset,
          assayName = assayName,
          qcName = "Normalized data",
          contrastName = contrastName,
          studies = studies, qcType = "assay",
          verbose = verbose,
          labelSamples = labelSamples)
      } else if(dataType == "mutation"){
        miodinDataset <- utilImporterProcessedMicroarrayMutation(
          datasetName = datasetName,
          assayTable = assayTable,
          assayName = assayName,
          studyName = studyName,
          sampleTable = sampleTable,
          chipName = chipName,
          projectPath = projectPath,
          probeAnnotationFile = probeAnnotationFile,
          workflowConfiguration = workflowConfiguration,
          verbose = verbose,
          moduleName = moduleName,
          fileFormat = fileFormat,
          useff = useff,
          orderedByFeature = orderedByFeature,
          excludeSamples = excludeSamples
        )
        #NB: add QC function
      } else
        stop("Unknown microarray data type ", dataType)
    } else if(experiment == "sequencing"){
      if(dataType != "rna")
        stop("Unknown sequencing data type ", dataType)
      miodinDataset <- utilImporterProcessedSequencingRNA(
        datasetName = datasetName,
        assayTable = assayTable,
        assayName = assayName,
        studyName = studyName,
        sampleTable = sampleTable,
        projectPath = projectPath,
        workflowConfiguration = workflowConfiguration,
        verbose = verbose,
        moduleName = moduleName,
        fileFormat = fileFormat,
        useff = useff,
        orderedByFeature = orderedByFeature,
        inputIdentifier = inputIdentifier,
        excludeSamples = excludeSamples
      )
      miodinDataset <- utilQualitySequencingRNA(
        miodinDataset = miodinDataset,
        assayName = assayName,
        qcName = "Count data",
        contrastName = contrastName,
        studies = studies,
        verbose = verbose,
        labelSamples = labelSamples)
    } else if(experiment == "mass spec"){
      if(!dataType %in% c("protein", "metabolite"))
        stop("Unknown mass spec data type ", dataType)
      miodinDataset <- utilImporterProcessedMassSpecProtein(
        datasetName = datasetName,
        assayTable = assayTable,
        assayName = assayName,
        studyName = studyName,
        sampleTable = sampleTable,
        projectPath = projectPath,
        workflowConfiguration = workflowConfiguration,
        verbose = verbose,
        moduleName = moduleName,
        fileFormat = fileFormat,
        useff = useff,
        orderedByFeature = orderedByFeature,
        inputIdentifier = inputIdentifier,
        excludeSamples = excludeSamples
      )
      miodinDataset <- utilQualityMassSpecProtein(
        miodinDataset = miodinDataset,
        assayName = assayName,
        qcName = "Normalized data",
        contrastName = contrastName,
        studies = studies,
        verbose = verbose,
        labelSamples = labelSamples)
    } else if(experiment == "olink"){
      if(dataType != "protein")
        stop("Unknown Olink data type ", dataType)
      miodinDataset <- utilImporterProcessedOlinkProtein(
        datasetName = datasetName,
        assayTable = assayTable,
        assayName = assayName,
        studyName = studyName,
        sampleTable = sampleTable,
        projectPath = projectPath,
        workflowConfiguration = workflowConfiguration,
        verbose = verbose,
        moduleName = moduleName,
        fileFormat = fileFormat,
        useff = useff,
        orderedByFeature = orderedByFeature,
        excludeSamples = excludeSamples
      )
      miodinDataset <- utilQualityMassSpecProtein(
        miodinDataset = miodinDataset,
        assayName = assayName,
        qcName = "Normalized data",
        contrastName = contrastName,
        studies = studies,
        verbose = verbose,
        labelSamples = labelSamples)
    } else if(experiment == "assay"){
      if(dataType != "generic")
        stop("Unknown assay data type ", dataType)
      miodinDataset <- utilImporterProcessedAssayGeneric(
        datasetName = datasetName,
        assayTable = assayTable,
        assayName = assayName,
        studyName = studyName,
        sampleTable = sampleTable,
        projectPath = projectPath,
        workflowConfiguration = workflowConfiguration,
        verbose = verbose,
        moduleName = moduleName,
        fileFormat = fileFormat,
        useff = useff,
        orderedByFeature = orderedByFeature,
        excludeSamples = excludeSamples
      )
    }
    
    #Return output ports
    return(list(
      dataset = list(
        mds = miodinDataset, assayName = assayName
      )
    ))
  } )

#' @title importProcessedData
#' 
#' @description
#' Workflow module for importing processed omics data.
#' 
#' @param name Name of the workflow module (character string).
#' @param experiment Omics experiment, \code{"microarray"},
#' \code{"sequencing"}, \code{"mass spec"}, \code{"olink"} or
#' \code{"assay"}.
#' @param dataType Omics data type. Must be \code{"rna"}, \code{"snp"},
#' \code{"methylation"}, \code{"protein"}, \code{"metabolite"}, or
#' \code{"generic"}.
#' @param studyName Name of the study in which assay is defined.
#' @param assayName Name of assay to import.
#' @param datasetName Name of output dataset.
#' @param contrastName Name of contrast in study design used to color sample
#' groups.
#' @param orderedByFeature In case of a single assay data file with one-column
#' format, this parameter specified whether the data is ordered by feature
#' rather than sample (default is \code{FALSE}).
#' @param useff Whether or not to use \code{ff} package logic for data import.
#' @param chipName Name of microarray platform, see \code{chipAnnotations()}.
#' @param probeAnnotationFile Path to custom probe annotation file.
#' @param probeSummarization Either 'transcript' (transcript cluster
#' summarization), or 'probeset' (probeset summarization).
#' @param methylValues Whether methylation data is in M- or Beta-values.
#' Must be either \code{"M"} or \code{"Beta"}.
#' @param inputIdentifier Type of feature identifier used for imported data.
#' Must be one of \code{"symbol"} (HNGC gene symbol), \code{"entrez"}
#' (Entrez gene ID), \code{"uniprot"} (UniProt accession) or \code{"ensg"}
#' (Ensembl gene ID).
#' @param labelSamples Whether or not to label samples in the QC PCA plot.
#' @param excludeSamples Character vector of samples to exclude.
#' 
#' @details
#' This module is used to import processed transcriptomics, proteomics, SNP
#' and methylation data. Note that both \code{DataFile} and \code{DataColumn}
#' columns must be provided in the assay table for processed omics data, where
#' \code{DataFile} names the file itself and \code{DataColumn} the column in
#' that file containing the data to import.
#' 
#' Arguments that are common to all data types include \code{experiment}
#' (the type of experiment), \code{dataType} (type of omics data),
#' \code{studyName} (name of study where the assay table is defined),
#' \code{assayName} (name of assay table to import), \code{datasetName}
#' (name of imported dataset) and \code{contrastName} (name of contrast
#' in study that defines which group each sample belongs to). All these
#' arguments are required, except for \code{contrastName}, which is optionally
#' used to color-code samples in quality control plots generated for imported
#' data. See the sections below for more information on assay data file formats,
#' microarray probe annotation and what is considered processed data for each
#' data type.
#' 
#' An optional parameter for transcriptomics and proteomics data is
#' \code{inputIdentifier}, which is used to specify the row identifier
#' of imported data. Accepted values are \code{"symbol"} (HGNC gene symbol),
#' \code{"entrez"} (Entrez gene ID), \code{"uniprot"} (Uniprot protein
#' identifier) and \code{"ensg"} (Ensembl gene ID). When provided,
#' input identifiers are mapped to HGNC gene symbols and Entrez gene IDs.
#' This makes them easier to interpret and compare across datasets, e.g.
#' between gene and protein data.
#' 
#' @section Format of the assay data files:
#' 
#' The module accepts five different formats for the assay data to import. (I)
#' The first is to provide a single text file with columns separated by tab,
#' containing variables (e.g. genes) on rows and samples in separate columns.
#' (II) The second option is to provide one file per sample, where each file
#' contains variables on rows and one column with data to import. (III) The
#' third option is to provide a single file with a single column containing data
#' for all variables and samples. When providing data in this way, it is
#' necessary to specify whether the data is sorted on samples (default) or
#' variables. Sorting on ariables can be specified by setting
#' \code{orderedByFeature=TRUE}.
#' 
#' The remaining two formats apply when multiple assay files are supplied, each
#' containing multiple samples. These files either have one column per sample as
#' in (I), or a single column with data for all samples as in (II).
#' 
#' @section Probe annotation:
#' 
#' If microarray data is imported and probes have not been annotated,
#' i.e. the rows in the data are probe IDs, it is possible to map probes by
#' setting \code{chipName} to the type of array used. By default, official array
#' annotation is downloaded from the Affymetrix or
#' Illumina server. The user many also supply a custom annotation file with
#' \code{probeAnnotationFile} to use instead. The point of annotating the probes
#' is to know to which transcript/SNP/CpG site the probes hybridize to.
#' 
#' In order to annotate Affymetrix data, it is necessary to provide login
#' credentials to the Affymetrix server. This can be obtained online by visiting
#' \url{http://www.affymetrix.com/analysis/index.affx} and clicking Register at
#' the top of the page. The credentials need to be added to the workflow
#' configuration like so.
#' 
#' \preformatted{
#' mw <- MiodinWorkflow("AnalysisFlow")
#' mw <- mw + workflowConfiguration(
#'   affyUser = "username",
#'   affyPass = "passwd")
#' }
#' 
#' In order to not save the password as text in the script, the \code{getPass}
#' package can be used.
#' 
#' \preformatted{
#' mw <- MiodinWorkflow("AnalysisFlow")
#' mw <- mw + workflowConfiguration(
#'   affyUser = "username",
#'   affyPass = getPass::getPass("Password"))
#' }
#' 
#' When using official annotation for Affymetrix Gene and Exon arrays, it is
#' also necessary to
#' specify whether the normalized data has been summarized on transcript
#' clusters (\code{probeSummarization="transcript"}) or on probesets
#' (\code{probeSummarization="probeset"}) in order to find the correct
#' annotation file. Transcript cluster summarization
#' implies that the rows of the expression data will correspond to different
#' transcripts. With probeset summarization, the rows will correspond to
#' individual exons of transcripts instead.
#' 
#' @section Importing transcriptomics data:
#' 
#' Transcriptomics data from both microarrays and RNA-seq are supported. This
#' is specified by setting \code{experiment="microarray"} or
#' \code{experiment="sequencing"} and \code{dataType="rna"}. For
#' microarrays, data should have been normalized across arrays to make samples
#' comparable. In case of RNA-seq, gene counts should be provided.
#' 
#' @section Importing proteomics data:
#' 
#' Import of continuous protein expression data is supported. This is specified
#' by setting \code{experiment="mass spec"} and \code{dataType="protein"}.
#' Data should have been normalized across samples to make them comparable.
#' 
#' @section Importing SNP data:
#' 
#' Import of genotyped SNP data is supported. This is specified
#' by setting \code{experiment="microarray"} and \code{dataType="snp"}.
#' Genotype calls can be provided in two formats. One option is to code
#' homozygous samples AA for the first allele and BB for the second allele.
#' Heterozygous samples are coded AB. The other option is to code genotype
#' by integers, where 1 denotes homozygosity for the first allele, 2 denotes
#' heterozygosity and 3 denotes homozygosity for the second allele. Everything
#' else is treated as missing values, which can also be coded as 0.
#' 
#' @section Importing methylation data:
#' 
#' Import of methylation data from Illumina arrays is supported. This is
#' specified by setting \code{experiment="microarray"} and
#' \code{dataType="methylation"}. Data should have been normalized across
#' arrays to make samples comparable. It is important to specify
#' whether imported data contain M-values (default) or Beta-values. This is
#' specified with \code{methylValues="M"} or \code{methylValues="Beta"},
#' respectively.
#' 
#' @author Benjamin Ulfenborg
#' 
#' @seealso \link{WorkflowModule-MicroarrayDataImporter}
#' \link{utilAssayFileReader}
#' 
#' @name InstatiateModule-importProcessedData
#' @export importProcessedData
importProcessedData <- function(name,
                                experiment = "",
                                dataType = "",
                                studyName = "",
                                assayName = "",
                                datasetName = "",
                                contrastName = NULL,
                                chipName = "",
                                probeAnnotationFile = NULL,
                                probeSummarization = "transcript",
                                orderedByFeature = FALSE,
                                useff = FALSE,
                                methylValues = "M",
                                inputIdentifier = "",
                                labelSamples = FALSE,
                                excludeSamples = NULL){
  mwm <- workflowModule("ProcessedDataImporter", name,
                        experiment = experiment,
                        dataType = dataType,
                        studyName = studyName,
                        assayName = assayName,
                        datasetName = datasetName,
                        contrastName = contrastName,
                        chipName = chipName,
                        probeAnnotationFile = probeAnnotationFile,
                        probeSummarization = probeSummarization,
                        orderedByFeature = orderedByFeature,
                        useff = useff,
                        methylValues = methylValues,
                        inputIdentifier = inputIdentifier,
                        labelSamples = labelSamples,
                        excludeSamples = excludeSamples)
  return(mwm)
}
