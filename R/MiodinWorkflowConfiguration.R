#Include MiodinWorkflow dependencies
#' @include standardGenerics.R
NULL

#' @title MiodinWorkflowConfiguration
#' 
#' @description
#' The \code{MiodinWorkflowConfiguration} class contains settings for
#' \code{MiodinWorkflow} objects that affect workflow execution.
#' 
#' \preformatted{## Class constructor
#' MiodinWorkflowConfiguration()}
#' 
#' @param object A \code{MiodinWorkflowConfiguration} object.
#' @param props Named list of properties to set (for \code{setProp}) or a
#' character vector (for \code{getProp}).
#' @param res Named used to return parameters.
#' 
#' @details
#' 
#' This class is used internally in a \code{MiodinWorkflow} and is never
#' explicitly created. For more details on workflow job deployment, see Nextflow
#' \href{https://www.nextflow.io/docs/latest/index.html}{reference documentation}.
#' 
#' @section Properties of \code{MiodinWorkflowConfiguration}:
#' 
#' The \code{setProp} and \code{getProp} methods accept the
#' following properties:
#' \itemize{
#'   \item \code{"affyUser"}: Affymetrix user ID as character string (deprecated).
#'   \item \code{"affyPass"}: Affymetrix password as character string (deprecated).
#'   \item \code{"parallelNumCores"}: Number of CPU cores to utilize for
#'   parallel processing.
#'   \item \code{"all"}: Retrieves all properties. Only applies to
#'   \code{getProp}.
#'   \item \code{"jobExecutor"}: Name of resource manager to submit a workflow
#'   job to.
#'   \item \code{"jobProject"}: Name of project in which a workflow job will be
#'   submitted.
#'   \item \code{"jobName"}: Name of workflow job.
#'   \item \code{"jobPartition"}: Workflow job partition, either \code{"core"}
#'   or \code{"node"}.
#'   \item \code{"jobNumCores"}: Number of CPU cores to allocate to
#'   workflow job.
#'   \item \code{"jobMemory"}: Amount of memory in GiB to allocate to workflow
#'   job.
#'   \item \code{"jobTime"}: Core hours to allocate to workflow job.
#'   \item \code{"jobEmail"}: Email used for notifications from workflow job.
#'   \item \code{"jobModules"}: Character vector of modules to load when
#'   starting a workflow job.
#'   \item \code{"jobContainer"}: Name of Docker container to use when deploying
#'   a workflow job.
#' }
#'
#' @slot parallelNumCores Number of CPU cores for parallel processing.
#' @slot jobExecutor Name of resource manager to submit a workflow job to.
#' @slot jobProject Name of project in which a workflow job will be submitted.
#' @slot jobName = Name of workflow job.
#' @slot jobPartition jobPartition Workflow job partition, either \code{"core"}
#' or \code{"node"}.
#' @slot jobNumCores Number of CPU cores to allocate to workflow job.
#' @slot jobMemory Amount of memory in GiB to allocate to workflow job.
#' @slot jobTime Core hours to allocate to workflow job.
#' @slot jobEmail Email used for notifications from workflow job.
#' @slot jobModules Character vector of modules to load when starting a workflow
#' job.
#' @slot jobContainer Name of Docker container to use when deploying a workflow
#' job.
#' 
#' @author Benjamin Ulfenborg
#' 
#' @seealso \link{MiodinWorkflow-class}
#' 
#' @name MiodinWorkflowConfiguration-class
#' @export MiodinWorkflowConfiguration
MiodinWorkflowConfiguration <- setClass(
  "MiodinWorkflowConfiguration",
  slots = list(
    affyUser = "character",
    affyPass = "character",
    parallelNumCores = "numeric",
    jobExecutor = "character",
    jobProject = "character",
    jobName = "character",
    jobPartition = "character",
    jobNumCores = "numeric",
    jobMemory = "numeric",
    jobTime = "character",
    jobEmail = "character",
    jobModules = "character",
    jobContainer = "character"
  ),
  prototype = list(
    affyUser = NULL,
    affyPass = NULL,
    parallelNumCores = NULL,
    jobExecutor = NULL,  # -> process.executor = 'slurm'
    jobProject = NULL,  # -> process.clusterOptions '-A'
    jobName = NULL,  # -> process.clusterOptions '-J'
    jobPartition = NULL,  # -> process.clusterOptions '-p'
    jobNumCores = NULL,  # -> process.cpus
    jobMemory = NULL, # -> process.memory
    jobTime = NULL,  # -> process.time
    jobEmail = NULL,  # -> process.clusterOptions '--mail-type=ALL'
                      #      '--mail-user=XXX'
    jobModules = NULL,  # -> process.module 'bioinfo-tools:R/3.5.0:
    #                          R_packages/3.5.0'
    jobContainer = NULL  # -> process.container = 'nextflow/examples:latest'
    #                         process.containerOptions =
    #                           '-v projectPath:projectPath'
    #                         docker.enable = true, 
  ),
  validity = function(object){
    errors <- character()
    if(length(errors) == 0)
      return(TRUE)
    else
      return(errors)
  },
  sealed = TRUE
)

#' @describeIn MiodinWorkflowConfiguration Sets workflow configuration settings.
setMethod("setProp", "MiodinWorkflowConfiguration",
  function(object, props){
    if(!class(props)[1] == "list") stop("props must be a list")
    if(length(props) == 0) stop("props must not be empty")
    if(is.null(names(props))) stop("props must be a named list")
    for(prop in names(props)){
      if(!(prop %in% c("affyUser", "affyPass", "parallelNumCores",
                       "jobExecutor", "jobProject", "jobName",
                       "jobPartition", "jobNumCores",
                       "jobMemory", "jobTime",
                       "jobEmail", "jobModules", "jobContainer")))
        stop("Unknown workflow configuration property ", prop)
      value <- props[[prop]]
      if(prop == "affyUser"){
        if(!is.character(value) || length(value) != 1)
          stop("affyUser must be a character string")
        object@affyUser <- value
      }
      if(prop == "affyPass"){
        if(!is.character(value) || length(value) != 1)
          stop("affyPass must be a character string")
        object@affyPass <- value
      }
      if(prop == "parallelNumCores"){
        if(!is.numeric(value))
          stop("parallelNumCores must be an integer")
        object@parallelNumCores <- floor(value)
      }
      if(prop == "jobExecutor"){
        if(!value %in% c("sge", "uge", "lsf", "slurm", "pbs",
                         "conder", "nqsii", "ignite", "k8s",
                         "awsbatch", "google-pipelines"))
          stop("Invalid jobExecutor specified")
        object@jobExecutor <- value
      }
      if(prop == "jobProject"){
        if(!is.character(value))
          stop("jobProject must be a character string")
        object@jobProject <- value
      }
      if(prop == "jobName"){
        if(!is.character(value))
          stop("jobName must be a character string")
        object@jobName <- value
      }
      if(prop == "jobPartition"){
        if(!value %in% c("core", "node"))
          stop("jobPartition must be 'core' or 'node'")
        object@jobPartition <- value
      }
      if(prop == "jobNumCores"){
        if(!is.numeric(value))
          stop("jobNumCores must be an integer")
        object@jobNumCores <- value
      }
      if(prop == "jobMemory"){
        if(!is.numeric(value))
          stop("jobMemory must be an integer")
        object@jobMemory <- value
      }
      if(prop == "jobTime"){
        if(!is.character(value) || !regexpr("[0-9]{1,2}:[0-9]{1,2}:[0-9]{1,2}",
                                            value, perl = TRUE))
          stop("jobTime must be in format XX:YY:ZZ")
        object@jobTime <- value
      }
      if(prop == "jobEmail"){
        if(!is.character(value))
          stop("jobEmail must be a character string")
        object@jobEmail <- value
      }
      if(prop == "jobModules"){
        if(!is.character(value))
          stop("jobModules must be a character vector")
        object@jobModules <- value
      }
      if(prop == "jobContainer"){
        if(!is.character(value))
          stop("jobContainer must be a character string")
        object@jobContainer <- value
      }
    }
    return(object)
  } )

#' @describeIn MiodinWorkflowConfiguration Returns workflow configuration
#' settings.
setMethod("getProp", "MiodinWorkflowConfiguration",
  function(object, props, res = list()){
    if(length(props) == 0)
      stop("props must not be empty")
    if(!is.character(props))
      stop("props must be a character vector")
    for(prop in props){
      if(!(prop %in% c("affyUser", "affyPass", "parallelNumCores", "all",
                       "jobExecutor", "jobProject", "jobName",
                       "jobPartition", "jobNumCores",
                       "jobMemory", "jobTime",
                       "jobEmail", "jobModules", "jobContainer")))
        stop("Unknown workflow configuration property ", prop)
      if(prop == "affyUser" || prop == "all")
        res[["affyUser"]] <- object@affyUser
      if(prop == "affyPass" || prop == "all")
        res[["affyPass"]] <- object@affyPass
      if(prop == "parallelNumCores" || prop == "all")
        res[["parallelNumCores"]] <- object@parallelNumCores
      if(prop == "jobExecutor" || prop == "all")
        res[["jobExecutor"]] <- object@jobExecutor
      if(prop == "jobProject" || prop == "all")
        res[["jobProject"]] <- object@jobProject
      if(prop == "jobName" || prop == "all")
        res[["jobName"]] <- object@jobName
      if(prop == "jobPartition" || prop == "all")
        res[["jobPartition"]] <- object@jobPartition
      if(prop == "jobNumCores" || prop == "all")
        res[["jobNumCores"]] <- object@jobNumCores
      if(prop == "jobMemory" || prop == "all")
        res[["jobMemory"]] <- object@jobMemory
      if(prop == "jobTime" || prop == "all")
        res[["jobTime"]] <- object@jobTime
      if(prop == "jobEmail" || prop == "all")
        res[["jobEmail"]] <- object@jobEmail
      if(prop == "jobModules" || prop == "all")
        res[["jobModules"]] <- object@jobModules
      if(prop == "jobContainer" || prop == "all")
        res[["jobContainer"]] <- object@jobContainer
    }
    return(res)
  } )
