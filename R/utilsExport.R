#' @title utilExport2Excel
#' 
#' @description
#' Function for exporting a list of data frames to Excel sheets.
#' 
#' @param dfList List of data frames to export.
#' @param sheetNames Vector of sheet names to use.
#' @param wbName Name of the Excel file.
#' @param tableStyle Logical indicating whether to style the results as a table.
#' @param type String specifying the style type, currently only supports
#' \code{"generic"} and \code{"edgeR"}.
#' 
#' @details
#' This function is a wrapper \code{openxlsx} and exports a list of data frames
#' as sheets in an Excel workbook.
#' 
#' @author Benjamin Ulfenborg
#' 
#' @export
utilExport2Excel <- function(dfList, sheetNames, wbName,
                             tableStyle = FALSE, type = "generic"){
  if(length(dfList) == 0) return()
  if(length(dfList) == 1 && is.null(nrow(dfList[[1]]))) return()
  if(length(dfList) == 1 && nrow(dfList[[1]]) == 0) return()
  type = type[1]
  wb <- openxlsx::createWorkbook()
  for(ind in seq_along(dfList)){
    if(nrow(dfList[[ind]]) == 0) next
    dupNames <- which(duplicated(tolower(colnames(dfList[[ind]]))))
    if(length(dupNames) > 0){
      dupNames <- unique(tolower(colnames(dfList[[ind]])[dupNames]))
      for(dupName in dupNames){
        dupInd <- grep(dupName, tolower(colnames(dfList[[ind]])))
        colnames(dfList[[ind]])[dupInd] <- make.unique(tolower(
          colnames(dfList[[ind]]))[dupInd])
      }
    }
    sheetName <- sheetNames[ind]
    invalidInd <- which(is.na(sheetName) | is.null(sheetName))
    if(length(sheetName) > 0)
      sheetName[invalidInd] <- seq_along(invalidInd)
    if(nchar(sheetName) > 31)
      sheetName <- substr(sheetName, 1, 31)
    openxlsx::addWorksheet(wb, sheetName)
    if(tableStyle)
      openxlsx::writeDataTable(wb, sheetName, dfList[[ind]], rowNames=TRUE,
                               tableStyle = "TableStyleMedium9")
    else
      openxlsx::writeDataTable(wb, sheetName, dfList[[ind]], rowNames=TRUE,
                               tableStyle="none", withFilter=FALSE)
    if(type == "edgeR"){
      openxlsx::addStyle(wb, sheetName,
                         style = openxlsx::createStyle(numFmt = "SCIENTIFIC"),
                         rows = 2:nrow(dfList[[ind]]),
                         cols = rep(7:8, length.out=nrow(dfList[[ind]])-1))
      openxlsx::setColWidths(wb, sheetName, cols = 1:8,
                             widths = c(20, 15, 15, 10, 10, 10, 10, 10))
    }
  }
  openxlsx::saveWorkbook(wb, file = wbName, overwrite = TRUE)
}
